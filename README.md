PRISE EN MAIN DE GIT

## [Lien vers la doc](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet "Markdown README") 

##TRAVAIL A FAIRE POUR LUNDI

- Ajouter un footer au master
- Pousser votre travail sur un repo distant vide (sans readme, merge, checkout, log, statut)
- Tester en poussant sur un repo distant (avec readme)
- Designer la barre de nav
- Designe le footer

##FINALEMENT

L'objectif est de taper les commandes usuelles de git (staging, branches, commit)

## Ecrire ici les problèmes rencontrés

- Si je veux prendre la dernière version de master et la travailler dans ma branche navigation, comment bien le faire ? 
- Lorsqu'on fait un merge, on nous demande de sauvegarder les current, incoming changes, les deux ou comparer... comment bien choisir pour ajouter les dernières modifications? Incoming j'imagine
- Je fait checkout dans ma branche cible et je fait un merge, après il me demande de faire un ADD avant d'un COMMIT ?? 

